FROM node:12-alpine

RUN npm install -g npm

WORKDIR /opt

COPY package.json /opt/

RUN npm install

COPY . .

CMD ["npm", "run", "test"]