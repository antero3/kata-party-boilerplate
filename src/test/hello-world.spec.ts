import { HelloWorld } from "../main/hello-world";

describe('Hello world', () => {
    it('says hello', () => {
        // Arrange
        const helloWorld = new HelloWorld();

        // Act
        const result = helloWorld.hello();

        // Assert
        expect(result).toEqual("Hello world")
    })
})
